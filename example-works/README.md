Sample works
------------

In this directory, please find a few scripts and other materials you can use as examples as you develop your own plays.

"Code Review, Forwards and Back"
--------------------------------

["Code Review: Forwards and Back"](https://www.harihareswara.net/sumana/2017/11/11/0) ([video](https://pyvideo.org/pygotham-2017/code-review-forwards-and-back.html)) is by Sumana Harihareswara and Jason Owen, and licensed CC-BY. (Some [making-of thoughts](https://www.harihareswara.net/sumana/2017/10/02/0).) We performed it at PyGotham 2017 and RubyConf 2018.

This directory holds the scripts for both versions.

*Length*: 22 minutes in the 2017 PyGotham version, about 35 minutes in the 2018 RubyConf version.

*Budget*: We spent zero on props (using a laptop, notebook, backpack, and phone we already had), recruited 3 people to help us for free (light, sound, and a very small acting part), and paid a director for about 8 hours of work.

*Sumana's wild guess of time spent writing/rehearsing/prepping*: 50 hours.

"Python Grab Bag: A Set of Short Plays"
---------------------------------------

["Python Grab Bag: A Set of Short Plays"](https://www.harihareswara.net/sumana/2018/10/06/0) ([video](https://pyvideo.org/pygotham-2018/python-grab-bag-a-set-of-short-plays.html)) is by Sumana Harihareswara and Jason Owen, and licensed CC-BY. (Some [making-of thoughts](https://www.harihareswara.net/sumana/2018/09/20/0).) We performed it at PyGotham 2018.

This directory holds the scripts and a handout we gave attendees; see the [blog post](https://www.harihareswara.net/sumana/2018/10/06/0) for slides that accompanied some plays.

*Length*: 40 minutes in total; each play was 10 seconds to 2 minutes long.

*Budget*: Around USD$1700 on a combination of props (especially perishables), maybe 10 hours of rehearsal space, and payment for about 20-25 hours of directing and 6 hours of work by a sound/light person (we also got a friend to act a small part for free, and bartered work hours for about 6-10 more hours of work by the sound/light person).

*Sumana's wild guess of time spent writing/rehearsing/prepping*: 140 hours.
