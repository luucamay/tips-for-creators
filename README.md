# Tips for new creators

Guidance and tips for people making plays and other art about programming. Currently focused on encouraging narrative, visual, and performance art submissions to [The Art of Python](https://us.pycon.org/2019/hatchery/artofpython/) and [!!Con](https://bangbangcon.com).

Check out our [guide for people creating plays](play-creation-guide.md) in case you've never written, acted in, or staged a play before!

Maintainers: @brainwane, @ertyseidohl, and @BrendanAdkins.